python convert.py coco-tiny-v3-relu.cfg coco-tiny-v3-relu_best.weights tflite_relu_yolov3.h5
python keras_to_tflite_quant.py tflite_relu_yolov3.h5 tflite_model.tflite

https://github.com/tomassams/docker-edgetpu-compiler
restart nhe