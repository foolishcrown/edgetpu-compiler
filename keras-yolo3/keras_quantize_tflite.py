import tensorflow as tf
import numpy as np
import glob 
#import cv2
#all_path = glob.glob('01/*.jpg')
def rep_data_gen():
    a = []
    for file_name in all_path[:500]:
        img = cv2.imread(file_name)
        img = cv2.resize(img, (416, 416))
        img = img / 255.0
        img = img.astype(np.float32)
        a.append(img)
    a = np.array(a)
    print(a.shape) # a is np array of 160 3D images
    img = tf.data.Dataset.from_tensor_slices(a).batch(1)
    for i in img.take(100):
        print(i)
        yield [i]

def representative_dataset_gen():
    for _ in range(250):
        yield [np.random.uniform(0.0, 1.0, size=(1, 416, 416, 3)).astype(np.float32)]
     
     
converter = tf.compat.v1.lite.TFLiteConverter.from_keras_model_file("tflite_relu_yolov3.h5")
converter.optimizations = [tf.lite.Optimize.DEFAULT]
converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]

converter.representative_dataset = representative_dataset_gen

converter.inference_input_type = tf.uint8
converter.inference_output_type = tf.uint8

tflite_quant_model = converter.convert()

with open('last_yolo.tflite','wb') as f:
    f.write(tflite_quant_model)

print("DONE")
    