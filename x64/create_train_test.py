from glob import glob
import random
import argparse
import math

parser = argparse.ArgumentParser(description='agrument parse')
parser.add_argument("-tp",'--test_percent', type=float,help='split train and test')

args = parser.parse_args()

type_accept = ["*.jpg","*.png","*.jpeg"]
all_file = []

for img_type in type_accept:
    all_file = all_file + glob(f"data/imgs/{img_type}")

random.seed(12345678912343)
# random
random.shuffle(all_file)
random.shuffle(all_file)
random.shuffle(all_file)
random.shuffle(all_file)
random.shuffle(all_file)
random.shuffle(all_file)


TEST_PERCENT = args.test_percent

print(TEST_PERCENT)

num_test = math.ceil(len(all_file)*TEST_PERCENT)

test_list = all_file[:num_test]
train_list = all_file[num_test:]

with open('data/train.txt','w') as f:
    for name in train_list:
        name_write = name.replace('\\', '/')
        f.writelines(name_write+'\n')

with open('data/test.txt','w') as f:
    for name in test_list:
        name_write = name.replace('\\', '/')
        f.writelines(name_write+'\n')

print("DONE")

