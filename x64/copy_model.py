from datetime import datetime

day_time = datetime.now()
current_time = str(day_time.strftime("%y_%m_%d_%H:%M"))
file_name = 'backup/coco-tiny-v3-relu_best.weights'

# file_name_write_model = f'model/model_{current_time}_best.weights' 
file_name_write_keras = f'../../model/avc_model.weights'
