import re

num_of_batch = 4000
low = int(num_of_batch*0.8)
high = int(num_of_batch*0.9)
with open('coco-tiny-v3-relu.cfg','r+') as f:
    text = f.read()
    text = re.sub('max_batches=[\d]+',f'max_batches={num_of_batch}', text)
    text = re.sub('steps=[\d]+,[\d]+',f'steps={low},{high}', text)
    f.seek(0)
    f.write(text)
    f.truncate()
